<?php


function geek_maieul_pre_liens($texte){
	$match = array();
	if (!preg_match("#->(?:art)?(?:article)?([0-9]+)\]#",$texte,$match)){ //trouer les liens  vers les articles, sans rien derrière
		return $texte;
	}
	$obsolete = sql_getfetsel('id_article_lie','spip_articles_lies',array('id_article='.$match[1],'type_liaison='.sql_quote('correction'))); // y a-t-il un article de remplacement lié ?	
	if ($obsolete){
		$texte = str_replace($match[1].']',$match[1].'?archive=oui]',$texte); 	// on 	
	}
	return $texte;
	 	
}

?>
