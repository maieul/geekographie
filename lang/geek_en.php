<?php
/** Fichier de langue de SPIP **/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'apropos'   =>'About',
    'articles_indirects_rubrique'   =>'Articles of the rubric',
    'articles_directs_rubrique'   =>'Articles directly in the rubric',
'aucun_article_direct'        =>'No article directly in the rubric',
    'credit_logo'		=>'Logo from',
	'etendre_indirect'    =>'Extend to the  articles directly in  the rubric',
    'eviter_hierarc'        => '(Go directly to article\'s content)',
	'hierarc_principale'=>'Main path',
	'hierarc_autres'    =>'Other paths',
	'hierarc_autre'    =>'Other path',
	"invites"	=> "Guests",
	'limiter_direct'   =>'Limit to the articles directly in the rubric',
	'maj'	=>'Upgrade',
	'obsolete'=>"This article is obsolete. See the new version:
	[@titre@->@id_article@].",
	'projet'   =>'@lang@ : last lines of the author',
	'rss'=>'RSS&nbsp;2.0',
	'rss_forum'=>'RSS&nbsp;2.0 Forum',
	'sous_rubriques'    =>'You can refine search through <a href="#@ancre@">navigation menu</a>',
	'succession_article'		=>'Articles sequence'
);

?>
