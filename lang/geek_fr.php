<?php
/** Fichier de langue de SPIP **/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'apropos'   =>'À propos',
    'articles_indirects_rubrique'   =>'Les articles de la rubrique',
    'articles_directs_rubrique'   =>'Les articles directement dans la rubrique',
'aucun_article_direct'        =>'Aucun article directement dans la rubrique',
    'credit_logo'		=>'Logo extrait de',
	'etendre_indirect'    =>'Étendre aux articles indirectement dans la rubrique',
    'eviter_hierarc'        => '(Aller directement au contenu de l\'article)',
	'hierarc_principale'=>'Chemin principal',
	'hierarc_autres'    =>'Autres chemins',
	'hierarc_autre'    =>'Autre chemin',
	"invites"	=> "Invités",
	"invites_introduction" => "Si [->auteur1] est l'auteur principal de ce site, il invite de temps en temps des auteurs à y écrire. Retrouvez-les ainsi que leurs œuvres.",
	'limiter_direct'   =>'Limiter aux articles directement dans la rubrique',
	
	
	'maj'	=>'mise à jour',
	'obsolete'=>"
	Cet article est obsolète. Soit qu'il existe une manière plus «&nbsp;moderne&nbsp;» de procéder, soit que j'ai trouvé une autre méthode plus appropriée.

	Voyez plutôt la nouvelle version : [@titre@->@id_article@].
	",
	'projet'   =>'@lang@ : dernières lignes de l\'auteur',
	'reorganisation'   =>'Suite à la réorganisation d\'août 2012, les articles listés ici se retrouvent dans l\'une des rubriques suivantes :',
	'rss'=>'RSS&nbsp;2.0',
	'rss_forum'=>'RSS&nbsp;2.0 Forum',
	'site_langage'    =>'Site à propos de @lang@',
	'sous_rubriques'    =>'Vous pouvez affiner la recherche via le <a href="#@ancre@">menu de navigation</a>',
	'succession_article'		=>'Succession d\'articles'
);

?>
